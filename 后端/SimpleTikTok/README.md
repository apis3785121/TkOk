# SimpleTikTok

#### 介绍
API大作业简易版抖音后端

#### 软件架构
SpringBoot+Mybatis+Redis+阿里云数据库+阿里云OSS

#### 使用说明

1.  启动前先打开Redis-x64-3.2.100文件夹里面的redis-server.exe
1.  在浏览器输入http://localhost:8081/swagger-ui/index.html访问swagger接口文档

