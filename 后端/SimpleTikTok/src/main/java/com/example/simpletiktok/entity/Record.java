package com.example.simpletiktok.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Record {
    private int userId;
    private int videoId;
    private LocalDateTime likeTime;// 点赞视频的时刻
    private LocalDateTime watchTime;// 观看视频的时刻
}
