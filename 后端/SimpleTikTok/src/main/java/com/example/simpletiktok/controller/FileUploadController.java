package com.example.simpletiktok.controller;


import com.example.simpletiktok.entity.Result;
import com.example.simpletiktok.utils.AliOssUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@RestController
@CrossOrigin
public class FileUploadController {
    // 上传文件返回文件在阿里云的url地址
    @PostMapping("/files")
    public Result<String> upload(MultipartFile file) throws Exception {
        // 把文件的内容储存到本地磁盘上
        String originalFilename = file.getOriginalFilename();
        // 保证文件名是唯一的，从而防止文件覆盖
        String filename = UUID.randomUUID() + originalFilename.substring(originalFilename.lastIndexOf("."));
        String url= AliOssUtils.uploadFile(filename,file.getInputStream());
        return Result.success(url);
    }
}
