package com.example.simpletiktok.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Video {
    private int id; // 视频id
    private int userId; // 上传用户id
    private String title;// 视频标题
    private String description; // 视频描述
    private String url; // 视频地址
    private LocalDateTime time; //视频发布时间
    private int like; // 点赞数
    public Video(int userId, String title, String description, String url, LocalDateTime time, int like) {
        this.userId = userId;
        this.title = title;
        this.description = description;
        this.url = url;
        this.time = time;
        this.like = like;
    }
}
