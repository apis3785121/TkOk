package com.example.simpletiktok.controller;


import com.example.simpletiktok.entity.Result;
import com.example.simpletiktok.entity.User;
import com.example.simpletiktok.service.UserService;
import com.example.simpletiktok.utils.JwtUtil;
import com.example.simpletiktok.utils.Md5Util;
import com.example.simpletiktok.utils.ThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@Validated
@CrossOrigin
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    // 需要以json格式传入username和password
    @PostMapping("/users")
    public Result register(@RequestBody Map<String,String> params){
        // 查询用户
        User u=userService.findByUserName(params.get("username"));
        if(u==null) {
            if(params.get("username")==null){
                return Result.error("用户名不能为空");
            }else if(params.get("password")==null){
                return Result.error("密码不能为空");
            }
            else if(params.get("nickname")==null){
                return Result.error("昵称不能为空");
            }
            // 注册
            userService.register(params.get("username"),params.get("password"),params.get("nickname"));
            return Result.success();
        }else {
            // 占用
            return Result.error("用户名已被占用");
        }
    }

    // 需要以json格式传入username和password
    @PostMapping("/sessions")
    public Result<String> login(@RequestBody Map<String,String> params){
        // 根据用户名查询用户
        User u=userService.findByUserName(params.get("username"));
        // 如果用户名或密码不对
        if(u!=null&& Md5Util.getMD5String(params.get("password")).equals(u.getPassword())){
            // 登录成功返回jwt令牌
            Map<String,Object>claims=new HashMap<>();
            claims.put("id",u.getId());
            claims.put("username",u.getUsername());
            String  token= JwtUtil.genToken(claims);
            // token存储到redis中
            ValueOperations<String, String> operations = stringRedisTemplate.opsForValue();
            operations.set(token,token,1, TimeUnit.HOURS);
            return Result.success(token);
        }else{
            return Result.error("用户名或密码错误");
        }
    }

    // 获取我的详细信息
    @GetMapping("/users/me")
    public Result<User> userInfo(){
        Map<String,Object>map= ThreadLocalUtil.get();
        int id = (int) map.get("id");//登录的用户名
        User user = userService.findById(id);
        return Result.success(user);
    }

    // 更新用户密码,需要以json格式传入old_pwd和new_pwd
    @PatchMapping("/users/me/password")
    public Result updatePsw(@RequestBody Map<String,String> params){
        String oldPwd=params.get("old_pwd");
        String newPwd=params.get("new_pwd");

        if(!StringUtils.hasLength(oldPwd)||!StringUtils.hasLength(newPwd)){
            Result.error("缺少必要参数");
        }

        // 原密码是否正确
        // 调用userService根据用户名拿到原密码，再和old_pws
        Map<String,Object> map = ThreadLocalUtil.get();
        int id = (Integer) map.get("id");
        User user=userService.findById(id);

        if(!user.getPassword().equals(Md5Util.getMD5String(oldPwd))){
            return Result.error("原密码错误");
        }

        userService.updatePwd(id,newPwd);
        return Result.success();
    }

    // 更新用户昵称,需要以json格式传入nickname
    @PatchMapping("/users/me/nickname")
    public Result updateNickname(@RequestBody Map<String,String> params){
        String nickname=params.get("nickname");
        if(!StringUtils.hasLength(nickname)){
            Result.error("nickname不能为空");
        }
        Map<String,Object> map = ThreadLocalUtil.get();
        int id = (Integer) map.get("id");
        User user=userService.findById(id);
        userService.updateNickname(id,nickname);
        return Result.success();
    }


}


