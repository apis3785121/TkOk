package com.example.simpletiktok.service.impl;

import com.example.simpletiktok.entity.Video;
import com.example.simpletiktok.mapper.RecordMapper;
import com.example.simpletiktok.mapper.VideoMapper;
import com.example.simpletiktok.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VideoServiceImpl implements VideoService {
    @Autowired
    private VideoMapper videoMapper;
    @Autowired
    private RecordMapper recordMapper;

    @Override
    public Video getVideoByUrl(String url) {
        return videoMapper.getVideoByUrl(url);
    }

    @Override
    public Video getVideoById(int id) {
        return videoMapper.getVideoById(id);
    }

    @Override
    public void addVideo(Video video) {
        videoMapper.addVideo(video.getUserId(),video.getTitle(),video.getDescription(),video.getUrl(), video.getLike());
    }

    @Override
    public void deleteVideo(int videoId) {
        videoMapper.deleteVideo(videoId);
    }

    @Override
    public List<Video> getVideosOfOne(int userId) {
        return videoMapper.getVideosOfOne(userId);
    }

    @Override
    public List<Video> getVideosByLike() {
        return videoMapper.getAllVideosByLike();
    }

    @Override
    public void addLike(int videoId) {
        videoMapper.addLike(videoId);
    }
}
