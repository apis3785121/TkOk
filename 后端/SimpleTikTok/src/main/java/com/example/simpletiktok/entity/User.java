package com.example.simpletiktok.entity;



import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;

import lombok.Data;



// lombok，编译阶段，自动为实体类添加setter、getter、toString方法
// pom文件中引入依赖，在实体类中添加注解
@Data
public class User {
    @NotNull
    private Integer id;//主键ID

    private String username;//用户名

    @JsonIgnore //将当前字符串转化为json字符串时，忽略password，最终的json字符串不带有password这个属性了
    // @JsonProperty("password")
    private String password;//密码


    private String nickname;//昵称

    public User(String username,String password){
        this.username=username;
        this.password=password;
    }
}

