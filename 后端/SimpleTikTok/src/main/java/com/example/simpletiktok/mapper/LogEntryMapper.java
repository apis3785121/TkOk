package com.example.simpletiktok.mapper;

import com.example.simpletiktok.entity.LogEntry;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LogEntryMapper {
    @Insert("INSERT INTO log (operator_id, operation_time, class_name, method_name, input_params, output_params, used_time) " +
            "VALUES (#{operatorId}, #{operationTime}, #{className}, #{methodName}, #{inputParams}, #{outputParams}, #{usedTime})")
    void insertLogEntry(LogEntry logEntry);
}
