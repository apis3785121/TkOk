package com.example.simpletiktok.service;


import com.example.simpletiktok.entity.User;

public interface UserService {
    // 注册
    void register(String username, String password, String nickname);

    // 根据用户名查询用户
    User findByUserName(String username);

    User findById(int id);

    // 更新
    void updateNickname(int id,String nickname);


    // 更新密码
    void updatePwd(int id,String newPwd);
}
