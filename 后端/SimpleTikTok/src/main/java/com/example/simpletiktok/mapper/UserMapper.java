package com.example.simpletiktok.mapper;


import com.example.simpletiktok.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface UserMapper {

    //根据用户名查询用户
    @Select("select * from user where username=#{username}")
    User findByUserName(String username);

    @Select("select * from user where id=#{id}")
    User findById(int id);

    //添加
    @Insert("insert into user(username,password,nickname)"+
            " values(#{username},#{password},#{nickname})"
    )
    void add(String username, String password, String nickname);

    //更新
    @Update("update user set nickname=#{nickname} where id=#{id}")
    void updateNickname(String nickname, Integer id);

    @Update("update user set password=#{newPwd} where id=#{id}")
    void updatePwd(String newPwd, Integer id);



}
