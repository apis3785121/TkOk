package com.example.simpletiktok.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogEntry {
    private Integer id;
    private Integer operatorId;
    private LocalDateTime operationTime;
    private String className;
    private String methodName;
    private String inputParams;
    private String outputParams;
    private Long usedTime;
}


