package com.example.simpletiktok.interceptors;
import com.example.simpletiktok.utils.JwtUtil;
import com.example.simpletiktok.utils.ThreadLocalUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import java.util.Map;

@Component
public class LoginInterceptor implements HandlerInterceptor {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 令牌验证
        String token=request.getHeader("Authorization");
        if(request.getMethod().equals("OPTIONS")){ // 直接响应数据 （***** 这里是最重要的if ***）
            return true;
        }
        //验证token
        try{
            // 从redis中获取相同的token
            ValueOperations<String, String> operations = stringRedisTemplate.opsForValue();
            String redisToken=operations.get(token);
            if(redisToken==null){
                // token已经失效了
                response.getWriter().write("Unauthorized: Token is missing");
                throw new RuntimeException();
            }

            Map<String,Object>claims= JwtUtil.parseToken(token);
            // 把业务数据存储到ThreadLocal中
            ThreadLocalUtil.set(claims);
            // 放行
            return true;
        }catch (Exception e){
            // http响应状态码为401
            response.setStatus(401);
            response.getWriter().write("Unauthorized: " + e.getMessage());
            //不放行
            return false;
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        // 用完后清空，防止内存泄漏
        ThreadLocalUtil.remove();
    }
}
