package com.example.simpletiktok.mapper;

import com.example.simpletiktok.entity.Record;
import com.example.simpletiktok.entity.Video;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface VideoMapper {
    // 根据视频id查找视频
    @Select("select * from video where id =#{videoId}")
    Video getVideoById(int videoId);

    // 根据视频url查找视频
    @Select("select * from video where url=#{url}")
    Video getVideoByUrl(String url);

    // 发布视频
    @Insert("insert into video(userId,title,description,url,time,`like`) values(#{userId},#{title},#{description},#{url},now(),#{like})")
    void addVideo(int userId, String title, String description, String url,int like);
    // 删除视频
    @Delete("delete from video where id =#{videoId}")
    void deleteVideo(int videoId);
    // 点赞量高到低的所有视频
    @Select("SELECT * FROM video ORDER BY `like` DESC")
    List<Video> getAllVideosByLike();

    // 获取某个用户已发布过的所有视频
    @Select("select * from video where userId=#{userId}")
    List<Video> getVideosOfOne(int userId);


    // 点赞视频
    @Update("UPDATE video SET `like` = `like` + 1 WHERE id = #{videoId}")
    void addLike(int videoId);
}
