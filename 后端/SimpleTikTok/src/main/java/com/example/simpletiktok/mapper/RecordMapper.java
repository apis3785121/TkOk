package com.example.simpletiktok.mapper;

import com.example.simpletiktok.entity.Record;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface RecordMapper {
    // 根据userId和videoId返回记录
    @Select("select * from record where userId=#{userId} and videoId=#{videoId}")
    Record getRecordByUidAndVid(int userId, int videoId);

    // 返回某个用户观看过的所有记录
    @Select("select * from record where userId=#{userId} and watchTime IS NOT NULL")
    List<Record> watchRecordsByUid(int userId);

    // 返回某个用户点赞过的所有记录
    @Select("select * from record where userId=#{userId} and likeTime IS NOT NULL")
    List<Record> likeRecordsByUid(int userId);

    // 增加点赞记录
    @Insert({
            "INSERT INTO record (userId, videoId, watchTime,likeTime) ",
            "VALUES (#{userId}, #{videoId}, NOW(),NOW()) ",
            "ON DUPLICATE KEY UPDATE ",
            "likeTime = IF(VALUES(likeTime) IS NULL, likeTime, VALUES(likeTime))"
    })
    void addLike(int userId, int videoId);

    // 增加观看记录
    @Insert({
            "INSERT INTO record (userId, videoId, watchTime) ",
            "VALUES (#{userId}, #{videoId}, NOW()) ",
            "ON DUPLICATE KEY UPDATE ",
            "watchTime = IF(VALUES(watchTime) IS NULL, watchTime, VALUES(watchTime))"
    })
    void addWatch(int userId, int videoId);
}
