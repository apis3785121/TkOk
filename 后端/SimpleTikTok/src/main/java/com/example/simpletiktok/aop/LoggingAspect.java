package com.example.simpletiktok.aop;

import com.example.simpletiktok.entity.LogEntry;
import com.example.simpletiktok.mapper.LogEntryMapper;
import com.example.simpletiktok.utils.ThreadLocalUtil;
import com.google.common.util.concurrent.RateLimiter;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Map;

@Aspect
@Component
public class LoggingAspect {

    private static final Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

    @Autowired
    private LogEntryMapper logEntryMapper;

    // 定义一个切点，匹配所有控制器方法
    @Pointcut("execution(* com.example.simpletiktok.controller..*(..))")
    public void controllerMethods() {}

    // 定义RateLimiter，设置每秒最多10个请求
    private static final RateLimiter rateLimiter = RateLimiter.create(10.0);

    // 环绕通知
    @Around("controllerMethods()")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
        if (!rateLimiter.tryAcquire()) {
            // 当获取令牌失败时，直接返回错误响应或抛出异常
            logger.warn("Rate limit exceeded for method: {}", joinPoint.getSignature().getName());
            throw new RuntimeException("Too many requests, please try again later.");
        }

        long startTime = System.currentTimeMillis();

        // 获取方法名和类名
        String methodName = joinPoint.getSignature().getName();
        String className = joinPoint.getSignature().getDeclaringTypeName();
        // 获取方法参数
        Object[] args = joinPoint.getArgs();
        String inputParams = args != null ? Arrays.toString(args) : "[]";

        logger.info("Request to method: {}, with arguments: {}", methodName, args);

        // 执行方法
        Object result;
        try {
            result = joinPoint.proceed();
        } catch (Throwable throwable) {
            logger.error("Exception in method: {}, with arguments: {}, exception: {}", methodName, args, throwable);
            throw throwable;
        }

        long elapsedTime = System.currentTimeMillis() - startTime;

        String outputParams = result != null ? result.toString() : "null";

        logger.info("Response from method: {}, with result: {}, elapsed time: {} ms", methodName, result, elapsedTime);
        Map<String, Object> map = ThreadLocalUtil.get();
        int userId;
        if (map != null) {
            userId = (Integer) map.get("id");
        } else {
            userId = 0;
        }

        // 保存日志到数据库
        LogEntry logEntry = new LogEntry(
                null,  // id 由数据库自动生成
                userId,  // operatorId
                LocalDateTime.now(),
                className,
                methodName,
                inputParams,
                outputParams,
                elapsedTime
        );

        logEntryMapper.insertLogEntry(logEntry);

        return result;
    }
}

