package com.example.simpletiktok.service.impl;


import com.example.simpletiktok.entity.User;
import com.example.simpletiktok.mapper.UserMapper;
import com.example.simpletiktok.service.UserService;
import com.example.simpletiktok.utils.Md5Util;
import com.example.simpletiktok.utils.ThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public void register(String username, String password, String nickname) {
        // 加密
        String md5Util= Md5Util.getMD5String(password);
        // 注册
        userMapper.add(username,md5Util,nickname);
    }

    @Override
    public User findById(int id) {
        User u=userMapper.findById(id);
        return u;
    }

    @Override
    public User findByUserName(String username) {
        User u=userMapper.findByUserName(username);
        return u;
    }

    @Override
    public void updateNickname(int id,String nickname) {
        userMapper.updateNickname(nickname,id);
    }

    @Override
    public void updatePwd(int id,String newPwd) {
        userMapper.updatePwd(Md5Util.getMD5String(newPwd),id);
    }
}
