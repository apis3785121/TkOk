package com.example.simpletiktok.controller;

import com.example.simpletiktok.entity.Record;
import com.example.simpletiktok.entity.Result;
import com.example.simpletiktok.entity.Video;
import com.example.simpletiktok.service.RecordService;
import com.example.simpletiktok.utils.ThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@CrossOrigin
public class RecordController {
    @Autowired
    private RecordService recordService;

    // 获取自己所有的点赞记录
    @GetMapping("/records/me/likes")
    public Result getAllLikeRecords(){
        Map<String,Object> map = ThreadLocalUtil.get();
        int userId = (Integer) map.get("id");
        List<Record> likeRecords=recordService.likeRecordsByUid(userId);
        return Result.success(likeRecords);
    }

    // 获取自己所有的已观看记录
    @GetMapping("/records/me/watchs")
    public Result getAllWatchRecords(){
        Map<String,Object> map = ThreadLocalUtil.get();
        int userId = (Integer) map.get("id");
        List<Record> watchRecords=recordService.watchRecordsByUid(userId);
        return Result.success(watchRecords);
    }


}
