package com.example.simpletiktok.service;

import com.example.simpletiktok.entity.Video;

import java.util.List;

public interface VideoService {
    // 根据url获取视频，以便于判断视频是否已发布过
    Video getVideoByUrl(String url);

    // 根据视频视频id获取视频
    Video getVideoById(int id);

    // 发布视频
    void addVideo(Video video);
    // 删除视频
    void deleteVideo(int videoId);
    // 根据用户id
    List<Video> getVideosOfOne(int userId);
    // 根据点赞数查看所有未观看过的视频
    List<Video> getVideosByLike();

    // 点赞视频
    void addLike(int videoId);
}
