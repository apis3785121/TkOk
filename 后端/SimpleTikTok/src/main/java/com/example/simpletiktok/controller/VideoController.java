package com.example.simpletiktok.controller;

import com.example.simpletiktok.entity.Record;
import com.example.simpletiktok.entity.Result;
import com.example.simpletiktok.entity.Video;
import com.example.simpletiktok.service.RecordService;
import com.example.simpletiktok.service.UserService;
import com.example.simpletiktok.service.VideoService;
import com.example.simpletiktok.utils.AliOssUtils;
import com.example.simpletiktok.utils.ThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

@RestController
@CrossOrigin
public class VideoController {
    @Autowired
    private VideoService videoService;
    @Autowired
    private RecordService recordService;
    @Autowired
    private UserService userService;

    // video传title,description即可
    // 新增视频
    @PostMapping("/videos")
    public Result addVideo(@RequestParam("title") String title,
                           @RequestParam("description") String description,
                           @RequestParam("file") MultipartFile file) throws Exception {
        Map<String,Object> map = ThreadLocalUtil.get();
        int userId = (Integer) map.get("id");

        String originalFilename = file.getOriginalFilename();
        String filename = UUID.randomUUID() + originalFilename.substring(originalFilename.lastIndexOf("."));
        String url = AliOssUtils.uploadFile(filename, file.getInputStream());

        Video video = new Video();
        video.setTitle(title);
        video.setDescription(description);
        video.setUrl(url);
        video.setUserId(userId);  // 用户就是当前登录的用户
        video.setLike(0);  // 点赞数为0

        if (videoService.getVideoByUrl(url) != null) {
            return Result.error("视频已发布，请勿重复操作");
        } else {
            videoService.addVideo(video);
            return Result.success();
        }
    }


    // 根据视频id删除视频
    @DeleteMapping("/videos/{id}")
    public Result deleteVideo(@PathVariable int id){
        Map<String,Object> map = ThreadLocalUtil.get();
        int userId = (Integer) map.get("id");
        Video video=videoService.getVideoById(id);
        if(video==null){
            return Result.error("视频不存在");
        }else if(video.getUserId()!=userId){
            return Result.error("权限不足");
        }else{
            videoService.deleteVideo(video.getId());
            return  Result.success();
        }
    }

    // 根据视频id获取视频
    @GetMapping("/videos/{id}")
    public Result getVideoById(@PathVariable int id){
        Video video=videoService.getVideoById(id);
        if(video==null){
            return Result.error("视频不存在");
        }else{
            return Result.success(video);
        }
    }

    // 获取点赞数从多到少且为未观看过的视频
    @GetMapping("/videos")
    public Result getVideos(@RequestParam(defaultValue = "like") String orderBy,
                            @RequestParam(defaultValue = "true") boolean watched) {
        if(!orderBy.equals("like")||watched){
            return Result.error("条件设置错误");
        }

        Map<String,Object> map = ThreadLocalUtil.get();
        int userId = (Integer) map.get("id");

        // 点赞数从多到少的视频
        List<Video> likeVideos = videoService.getVideosByLike();
        // 点赞数从多到少且为未观看过的视频
        List<Video> videosOrderByLikeAndNotWatched=new ArrayList<>();
        for(Video video:likeVideos){
            // 过滤掉已查看的视频
            if(recordService.getRecordByUidAndVid(userId, video.getId())==null){
                videosOrderByLikeAndNotWatched.add(video);
                if(videosOrderByLikeAndNotWatched.size() == 5)
                {
                    break;
                }
            }
        }
        return Result.success(videosOrderByLikeAndNotWatched);
    }

    // 增加点赞视频记录
    @PostMapping("/videos/{id}/like")
    public Result likeVideo(@PathVariable int id) {
        Video video = videoService.getVideoById(id);
        Map<String, Object> map = ThreadLocalUtil.get();
        int userId = (Integer) map.get("id");

        if (video == null) {
            return Result.error("视频不存在");
        } else {
            Record record = recordService.getRecordByUidAndVid(userId, id);
            if (record != null && record.getLikeTime() != null) {
                return Result.error("已点赞过该视频");
            }
        }
        videoService.addLike(id);
        recordService.addLike(userId,id);
        return Result.success();
    }

    // 增加观看视频记录
    @PostMapping("/videos/{id}/record")
    public Result watchVideo(@PathVariable int id){
        Video video = videoService.getVideoById(id);
        Map<String, Object> map = ThreadLocalUtil.get();
        int userId = (Integer) map.get("id");

        if (video == null) {
            return Result.error("视频不存在");
        } else {
            Record record = recordService.getRecordByUidAndVid(userId, id);
            if (record != null && record.getWatchTime() != null) {
                return Result.error("已观看过该视频");
            }
        }
        recordService.addWatch(userId,id);
        return Result.success();
    }


    // 查看我已发布过的所有视频
    @GetMapping("/videos/user/me")
    public Result getVideosOfMyself(){
        Map<String, Object> map = ThreadLocalUtil.get();
        int userId = (Integer) map.get("id");
        List<Video> videosOfMyself=videoService.getVideosOfOne(userId);
        return Result.success(videosOfMyself);
    }

    // 查看某个用户已发布过的所有视频
    @GetMapping("/videos/user/{userId}")
    public Result getVideosOfMyself(@PathVariable int userId){
        if(userService.findById(userId)==null){
            return Result.error("用户不存在");
        }
        List<Video> videosOfSomeone=videoService.getVideosOfOne(userId);
        return Result.success(videosOfSomeone);
    }

    @GetMapping("/records/me/history")
    public Result getAllHistoryRecords(){
        Map<String,Object> map = ThreadLocalUtil.get();
        int userId = (Integer) map.get("id");
        List<Record> watchRecords=recordService.watchRecordsByUid(userId);

//获取指定用户视频id不重复的观看记录
        List<Record> distinctRecords = new ArrayList<>();
        Set<Integer> visitedVideoIds = new HashSet<>();

        for (Record record : watchRecords) {
            if (!visitedVideoIds.contains(record.getVideoId())) {
                distinctRecords.add(record);
                visitedVideoIds.add(record.getVideoId());
            }
        }

// 视频的观看历史记录
        List<Video> historyVideos =new ArrayList<>();

        for(Record record:distinctRecords){
// 获取用户的观看历史记录
            if(videoService.getVideoById(record.getVideoId()) != null){
            historyVideos.add(videoService.getVideoById(record.getVideoId()));}
        }
        return Result.success(historyVideos);

    }
}
