package com.example.simpletiktok.service.impl;

import com.example.simpletiktok.entity.Record;
import com.example.simpletiktok.mapper.RecordMapper;
import com.example.simpletiktok.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecordServiceImpl implements RecordService {
    @Autowired
    private RecordMapper recordMapper;

    @Override
    public Record getRecordByUidAndVid(int userId, int videoId) {
        return recordMapper.getRecordByUidAndVid(userId,videoId);
    }

    @Override
    public List<Record> watchRecordsByUid(int userId) {
        return recordMapper.watchRecordsByUid(userId);
    }

    @Override
    public List<Record> likeRecordsByUid(int userId) {
        return recordMapper.likeRecordsByUid(userId);
    }

    @Override
    public void addLike(int userId, int videoId) {
        recordMapper.addLike(userId,videoId);
    }

    @Override
    public void addWatch(int userId, int videoId) {
        recordMapper.addWatch(userId,videoId);
    }
}
