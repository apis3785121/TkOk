package com.example.simpletiktok.service;
import com.example.simpletiktok.entity.Record;
import java.util.List;

public interface RecordService {
    Record getRecordByUidAndVid(int userId,int videoId);
    List<Record> watchRecordsByUid(int userId);
    List<Record> likeRecordsByUid(int userId);
    void addLike(int userId,int videoId);

    void addWatch(int userId, int videoId);
}
