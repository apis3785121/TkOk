package com.example.simpletiktok;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleTikTokApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleTikTokApplication.class, args);
    }

}
