const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  devServer: {
    overlay: {
      warnings: false,
      errors: false
    },
    lintOnSave: false
  },
  lintOnSave: false,
  transpileDependencies: true,
  devServer:{
    port: 7000
  }
})
