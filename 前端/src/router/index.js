import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/dept',
    name: 'dept',
    component: () => import('../views/tlias/TikView.vue')
  },
  {
    path: '/sole',
    name: 'sole',
    component: () => import('../views/tlias/SoleView.vue')
  },
  {
    path: '/sign',
    name: 'sign',
    component: () => import('../views/tlias/SignIn.vue')
  },
  {
    path: '/create',
    name: 'create',
    component: () => import('../views/tlias/CreateAccount.vue')
  },
  {
    path: '/emp',
    name: 'emp',
    component: () => import('../views/tlias/EmpView.vue')
  },
  {
    path: '/mine',
    name: 'mine',
    component: () => import('../views/tlias/Mine.vue')
  },
  {
    path: '/history',
    name: 'history',
    component: () => import('../views/tlias/History.vue')
  },
  {
    path: '/',
    redirect: '/dept'
  }
]

const router = new VueRouter({
  routes
})

export default router
